#include <stdio.h>
#define MAX 3 /* Define la dimension de la matriz */

void show(int matrix[][MAX]);
void llenar_esperilicamente(int matrix[][MAX]);

int main(int argc, char *argv[])
{
    int matrix[MAX][MAX];

    llenar_esperilicamente(matrix);
    show(matrix);

    return 0;
}

void show(int matrix[][MAX])
{
    /* imprime la matriz recibida por parametro en la pantalla */
    int i, j;

    for (i = 0; i < MAX; i++)
    {
        for (j = 0; j < MAX; j++)
        {
            printf("%4d", matrix[i][j]);
        }
        printf("\n");
    }
}

void llenar_esperilicamente(int matrix[][MAX])
{
    /* Llena la matriz del parametro desde adentro hacia afuera */
    int i, j;
    int count = 1;
    int aux;

    if (MAX % 2 != 0)
    {
        aux = MAX / 2; /* se comienza en el punto central de la matriz */
        for (i = 0; i < MAX / 2 + 1; i++)
        {
            for (j = aux + i; j > aux - (2 + i); j--)
            {
                if (count == MAX * MAX + 1)
                {
                    break;
                }
                matrix[aux - i][j] = count;
                count++;
            }
            if (count == MAX * MAX + 1)
            {
                break;
            }
            for (j = aux - (1 + i); j < aux + i; j++)
            {
                matrix[j + 2][aux - (1 + i)] = count;
                count++;
            }
            for (j = aux - (1 + i); j < aux + (1 + i); j++)
            {
                matrix[aux + (1 + i)][j + 1] = count;
                count++;
            }
            for (j = aux + i; j > aux - (1 + i); j--)
            {
                matrix[j][aux + (1 + i)] = count;
                count++;
            }
        }
    }
    else
    {
        aux = MAX / 2 - 1; /* se comienza en el punto central de la matriz */
        for (i = 0; i < MAX / 2 + 1; i++)
        {
            for (j = aux - i; j < aux + (2 + i); j++)
            {
                matrix[aux + (1 + i)][j] = count;
                count++;
            }
            for (j = aux + i; j > aux - (1 + i); j--)
            {
                matrix[j][aux + (1 + i)] = count;
                count++;
            }
            for (j = aux + i; j > aux - (2 + i); j--)
            {
                if (count == MAX * MAX + 1)
                {
                    break;
                }
                matrix[aux - i][j] = count;
                count++;
            }
            if (count == MAX * MAX + 1)
            {
                break;
            }
            for (j = aux - (1 + i); j < aux + i; j++)
            {
                matrix[j + 2][aux - (1 + i)] = count;
                count++;
            }
        }
    }
}
