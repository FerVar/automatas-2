#include<iostream>
#include<conio.h>
#include<stdlib.h>
using namespace std;



struct Nodo{
	char dato;
	Nodo *siguiente;
};



void agregar(Nodo *&, char);
void sacar(Nodo *&, char &);
char compara(Nodo *&);
void size(Nodo *&);



int main(){

	Nodo *pila = NULL;
	int tam = 0;
	string cadena;
	char dato, aux;
	
	
	
	cout<<"Digite los parentesis a evaluar: ";
	cin>> cadena;
	
	for(int i = 0; i < cadena.size(); i ++){
		dato = cadena[i];
		if(pila == NULL){
			agregar(pila, dato);
		}else{
			aux = compara(pila);
			if(aux == '(' && dato == ')'){
				sacar(pila, dato);
			}else{
				agregar(pila, dato);
			}
		}
	}
	
	size(pila);
	
	if(pila != NULL){
		cout<<"\nLos datos que se quedaron en la pila son: ";
		while(pila != NULL){
			sacar(pila,dato);
			cout<<dato<<"  ";
		}
	}

	
	getch();
	return 0;
}


void agregar(Nodo *&pila, char n){
	Nodo *nuevo_nodo = new Nodo();
	nuevo_nodo -> dato = n;
	nuevo_nodo -> siguiente = pila;
	pila = nuevo_nodo;
	
}


void sacar(Nodo *&pila, char &n){
	Nodo *aux = pila;
	n = aux -> dato;
	pila = aux -> siguiente;
	delete aux;
}

char compara(Nodo *&pila){
	char n;
	Nodo *aux = pila;
	n = aux -> dato;
	return n;
}

void size(Nodo *&pila) {
  Nodo *aux = pila;
  if (pila == NULL) {
    cout << "\n\t**La pila esta vacia.**\n\t**Validacion terminada**\n\n";
  } else {
    int contador = 0;
    while (aux) {
      contador++;
      aux = aux->siguiente;
    }
    if(contador != 0)
		cout<<"\nLa validacion termino con "<<contador<<" datos en la pila."<<endl;
  }
}
