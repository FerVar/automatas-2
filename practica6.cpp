#include <stdio.h>
#include <stdlib.h>
#include <string.h>

using namespace std;

int main(){

	//Arreglo en donde se guarda el codigo
	char cod[1000];
	
	//Estructura para abrir un fichero
	FILE *f;
	
	//fopen('nombre del archivo', 'r | w')
	//r = lectura | w = Escritura 
	f = fopen("codigoejemplo.txt","r");
	if(f == NULL){
		printf("No se ha podido abrir el archivo \n");
		exit(1);
	}
	
	//feof sirve para abrir
	while(!feof(f)){
		//fgets guarda lo que hay en el txt en
		//un arreglo (cod)
		//f es el archivo de donde esta leyendo
		fgets(cod, 1000, f);
	}
	
	//Crear token
    char *token; 
    char *rest = cod; 
  	int contador = -1;
	
	//Delimitador para encontrar asignaciones
	while ((token = strtok_r(rest, "=", &rest))) {
  		contador++;
	}
	
    printf("Numero de asignaciones: %d.\n",contador);
	
	return(0); 
	
}
