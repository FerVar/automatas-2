#include <iostream>
using namespace std;
 
struct Nodo {
    char c;
    Nodo *lo, *eq, *hi;
};
 
void agregar(Nodo *&nodo, string& s, int p) {
    if (p == s.size()) return;
    if (nodo == NULL) {
        nodo = new Nodo;
        nodo->c = s[p];
        nodo->lo = nodo->hi = nodo->eq = NULL;
        add(nodo->eq, s, p + 1);
    }
    else {
        if (s[p] < nodo->c) add(nodo->lo, s, p);
        else if (s[p] > nodo->c) add(nodo->hi, s, p);
        else add(nodo->eq, s, p + 1);
    }
}
 
bool buscar(Nodo *nodo, string& s, int p) {
    if (p == s.size()) return true;
    if (nodo == NULL) return false;
    if (s[p] < nodo->c) return search(nodo->lo, s, p);
    else if (s[p] > nodo->c) return search(nodo->hi, s, p);
    else return search(nodo->eq, s, p + 1);
}


void preorden(nodo):
    print(self.clave)
    if nodo.hijoIzquierdo:
        nodo.hijoIzquierdo.preorden()
    if nodo.hijoDerecho:
        nodo.hijoDerecho.preorden()
 
int main() {
    Nodo *raiz = NULL;
    char c;
    while (cin >> c) {
        if (c == 'A') {
            string s;
            cin >> s;
            add(raiz, s, 0);
        }
        else if (c == 'S') {
            string s;
            cin >> s;
            if (search(raiz, s, 0)) cout << "cadena encontrada" << endl;
            else cout << "cadena NO encontrada" << endl;
        }
    }
}
