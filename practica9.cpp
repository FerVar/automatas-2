#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main()
{

	char pal_res[35][10] =
		{
			"auto", "break", "case", "char",
			"const", "continue", "default", "do",
			"double", "else", "enum", "extern",
			"float", "for", "goto", "if",
			"int", "long", "main", "NULL", "register", "return",
			"short", "signed", "sizeof", "static",
			"struct", "switch", "typedef", "union",
			"unsigned", "void", "volatile", "while",
			"include"};

	char codigo_fuente[1000];

	FILE *f;

	f = fopen("codigop9.txt", "r");
	if (f == NULL)
	{
		printf("No se ha encontrado el archivo\n");
		exit(1);
	}

	while (!feof(f))
	{

		fgets(codigo_fuente, 1000, f);
	}

	char *token;
	char *rest = codigo_fuente;

	int contador[35] = {0};
	int c = 0, palabra = 1;

	while ((token = strtok_r(rest, " ", &rest)))
	{
		for (int i = 0; i < 35; i++)
		{

			if (strcmp(token, pal_res[i]) == 0)
			{
				c++;
				contador[i] += palabra;
			}
		}
	}

	printf("******Practica 9******\n\n");
	printf("Numero de palabras reservadas encontradas: %d\n\n", c);

	for (int i = 0; i < 35; i++)
	{
		printf("La palabra reservada '%s' se encontro %d veces.\n", pal_res[i], contador[i]);
	}
	printf("\n\nTermino la busqueda.\n");

	return 0;
}
